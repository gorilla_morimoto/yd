package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

func main() {
	files := findGoFiles()

	for _, v := range files {
		if hasInOut(v) {
			run(v)
		}
	}
}

func run(name string) {
	cmd := exec.Command("go", "run", name)
	stdin, _ := cmd.StdinPipe()

	go func() {
		defer stdin.Close()
		b, _ := (ioutil.ReadFile("in" + trimExt(name)))
		io.WriteString(stdin, fmt.Sprintf("%s", b))
	}()

	out, _ := cmd.CombinedOutput()
	ans, _ := ioutil.ReadFile("out" + trimExt(name))

	if string(out) != string(ans) {
		fmt.Println(
			"FAILED:", name, "->",
			"Expected:", strings.TrimSuffix(string(ans), "\n"),
			"Acutually:", strings.TrimSuffix(string(out), "\n"),
		)
	} else {
		fmt.Println("SUCCESS:", name)
	}
}

func findGoFiles() []string {
	files := make([]string, 0)
	f, _ := ioutil.ReadDir(".")

	for _, v := range f {
		if filepath.Ext(v.Name()) == ".go" {
			files = append(files, v.Name())
		}
	}

	return files
}

func hasInOut(goFile string) bool {
	base := trimExt(goFile)

	_, err := os.Stat("in" + base)
	if err != nil {
		return false
	}

	_, err = os.Stat("out" + base)
	if err != nil {
		return false
	}

	return true
}

func trimExt(orig string) string {
	ext := filepath.Ext(orig)
	return strings.TrimSuffix(orig, ext)
}
